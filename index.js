var mineflayer = require('mineflayer');
var waitUntil = require('wait-until');
const chalk = require("chalk");
const yaml = require("js-yaml");
const fs = require("fs");

process.title = "Canabutter Button Bot";

console.log("hi")

let config = yaml.load(fs.readFileSync(`${process.cwd()}/config.yaml`, "utf8"));

const wait = (waitTimeInMs) =>
  new Promise((resolve) => setTimeout(resolve, waitTimeInMs));

var options = {
  auth: "microsoft",
  username: "markjsercombe@outlook.com",
  host: config.minecraft.serverIP,
  port: config.minecraft.serverPort,
  version: config.minecraft.version,
};

var bot = mineflayer.createBot(options);
bindEvents(bot);

function bindEvents(bot) {
    bot.on('login', function() {
      bot.settings.viewDistance = "tiny";
      console.log(`  [${chalk.hex("#00FF00").bold("Login")}]: ${bot.username}`);
      wait(config.settings.joinCommandDelay * 1000).then(() => {
        bot.chat(config.minecraft.joinCommand);
      });
    });

    bot.on('spawn', function() {
        console.log("Bot has spawned");
    });

    bot.on('kicked', function(reason) {
        console.log("Kicked for ", reason);
    });

    bot.on('end', function(reason) {
        // Wait 10 seconds between tries, and try 9999 times
        waitUntil(10000, 9999, function condition() {
          try {
            console.log("Bot ended, attempting to reconnect...");
                bot = mineflayer.createBot(options);
                bindEvents(bot);
                return true;
           } catch (error) {
                console.log("Error: " + error);
                return false;
            }
            // Callback function that is only executed when condition is true or time allotted has elapsed
        }, function done(result) {
            console.log("Connection attempt result was: " + result);
        });
   });
}